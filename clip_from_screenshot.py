# -*- coding: utf-8 -*-
from PIL import ImageGrab
from PIL import Image


# 枠線（グレー）までのピクセル長を取得する
def find_gray(img, max_length, point, step):
    x, y = point
    xx, yy = step
    for i in range(max_length):
        r, g, b = img.getpixel((x + (xx * i), y + (yy * i)))
        if r == 0xc0 and g == 0xc0 and b == 0xc0:
            return i
    return max_length


def find_right_end(img, margins, empty_width):
    top, bottom, left = margins

    empty_count = 0
    y_max = img.height - (top + bottom)
    y = 0
    for x in range(img.width - left):
        for y in range(y_max):
            p = (x + left, y + top)
            r, g, b = img.getpixel(p)
            if r == g and g == b and b == r:
                continue
            else:
                break

        if y == y_max - 1:
            empty_count = empty_count + 1
            if empty_count >= empty_width:
                return img.width - x
        else:
            empty_count = 0

    return 0


# メインディスプレイのスクリーンショットを撮る
img = ImageGrab.grab()

# 上の余白サイズを取得する
max_length = img.height / 2
point = (img.width / 2, 0)
step = (0, +1)
top_margin = find_gray(img, max_length, point, step)

# 下の余白サイズを取得する
point = (img.width / 2, img.height - 1)
step = (0, -1)
bottom_margin = find_gray(img, max_length, point, step)

# 左の余白サイズを取得する
point = (0, img.height / 2)
step = (+1, 0)
left_margin = find_gray(img, max_length, point, step)

# 右の余白サイズを取得する
margins = (top_margin, bottom_margin, left_margin)
empty_width = 50
right_margin = find_right_end(img, margins, empty_width)

# 余白を切り取った画像を作成する
clip_width = 10
clip_height = 10
clip_rect = (left_margin, top_margin, img.width - right_margin, img.height - bottom_margin)
clipped = img.crop(clip_rect)
clipped.save('clipped.png')
