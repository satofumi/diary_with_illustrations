# -*- coding: utf-8 -*-
import os
import sys
import yaml
import shutil
import codecs
import datetime
from PIL import Image


_rank_text = ''


def _place_header(title):
    return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">' + \
        '<html>' + \
        '<head>' + \
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' + \
        '<link rel="stylesheet" type="text/css" href="default.css">' + \
        '<title>' + title + '</title>' + \
        '</head>' + \
        '<body>'


def _parse_rank_icon(name):
    tokens = name.split()
    tokens.append('')
    global _rank_text
    _rank_text = '&nbsp;(<img src="' + tokens[0] + '.png" width="24" height="24" alt="' + tokens[0] + '">' + tokens[1] + ')'


def _place_links(previous_page, next_page):
    output = ''
    output += '<div class="page_links">' + \
              u'<a href="index.html">もくじへ</a>'

    if previous_page:
        output += u' <a href="' + previous_page + u'">←まえの日へ</a>'

    if next_page:
        output += u' <a href="' + next_page + u'">つぎの日へ→</a>'

    output += '</div>'
    return output


def _place_date(month, day, name):
    return '<span class="date">' + str(month) + u' がつ &nbsp;' + str(day) + u' にち &nbsp; なまえ &nbsp;' + name + _rank_text + '</span>'

def _place_footer():
    return '</body>' + \
        '</html>'


def _place_image(image_folder, image_file, output_folder):
    if image_folder == None or image_file == None:
        return ''

    try:
        file_path = os.path.join(image_folder, image_file)
        shutil.copyfile(file_path, os.path.join(output_folder, image_file))

        img = Image.open(file_path)
    except:
        print('NoImageError: "' + file_path + '"')
        return ''

    width, height = img.size
    return '<img class="image" src="' + image_file + '" width="' + str(width) + '" height="' + str(height) + '"><br>'


def _icon_img(name, additional_class):
    icon_name = name.replace("'", '').replace(u'ú', 'u')
    if os.path.exists('html/icons/' + icon_name + '.png'):
        return '<img class="hero' + additional_class + '" src="icons/' + icon_name + '.png">'
    else:
        return ' <span class="hero">' + icon_name + '</span> '


def _hero_icon(draft, players):
    if draft['_event'] == 'NNet.Replay.Tracker.SHeroBannedEvent':
        return _icon_img(draft['m_hero'], ' ban')
    else:
        player_id = int(draft['m_controllingPlayer'])
        player = players[player_id]
        return _icon_img(player['hero'], '')


def _place_draft(file_name, player_name):
    with open('draft/' + file_name) as f:
        try:
            data = yaml.safe_load(f.read().decode('utf8'))
        except:
            import traceback
            sys.stderr.write('DiaryFormatError: "' + input_file + '"\n')
            traceback.print_exc(0)
            return

    output = ''
    #output += _place_title(data['map_name'])

    nickname = player_name.split('#')[0]
    drafts = data['drafts']
    players = data['players']
    above_team_id = data['above_team_id']
    above_team = 'team1' if above_team_id == 0 else 'team2'
    below_team = 'team1' if above_team_id != 0 else 'team2'

    for i,player in players.items():
        if player['name'] == nickname:
            own_team_id = player['team']

    if above_team_id == own_team_id:
        above_team_name = u'みかた'
        below_team_name = u'あいて'
    else:
        above_team_name = u'あいて'
        below_team_name = u'みかた'

    output += '<table class="draft"><tr><th class="draft_team">&nbsp;</th><th class="select" colspan="2">1st BAN</th><th class="select" colspan="2">2nd BAN</th><th class="select" colspan="3">Pick</th><th class="select" colspan="2">3rd BAN</th><th class="select" colspan="3">Pick</th></tr>'

    output += u'<tr class="{}"><td class="draft_team">{}</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td>&nbsp;</td></tr>'.format(above_team, above_team_name, _hero_icon(drafts[0], players), _hero_icon(drafts[2], players), _hero_icon(drafts[4], players), _hero_icon(drafts[7], players) + _hero_icon(drafts[8], players), _hero_icon(drafts[10], players), _hero_icon(drafts[13], players) + _hero_icon(drafts[14], players))

    output += u'<tr class="{}"><td class="draft_team">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td><td class="select">&nbsp;</td><td class="select double">{}</td><td class="select">&nbsp;</td><td class="select">{}</td></tr></table>'.format(below_team, below_team_name, _hero_icon(drafts[1], players), _hero_icon(drafts[3], players), _hero_icon(drafts[5], players) + _hero_icon(drafts[6], players), _hero_icon(drafts[9], players), _hero_icon(drafts[11], players) + _hero_icon(drafts[12], players), _hero_icon(drafts[15], players))

    return output


def _place_title(title):
    if title == None:
        title = u'たいとる まだ'

    return '<hr><h2>' + title + '</h2>'


def _place_text(texts):
    if texts == None:
        return ''

    output = '<p class="block">'
    for text in texts:
        output += text + '<br>'
    output += '</p>'
    return output


def _place_link(items):
    tokens = items.split(',')
    if len(tokens) == 1:
        link, name = tokens[0], tokens[0]
    elif len(tokens) == 2:
        link, name = tokens[1], tokens[0]

    return '<a class="link" href="' + link + '">' + name + '</a> '


def _parse_date(file_name):
    year = int(file_name[-14:-10])
    month = int(file_name[-9:-7])
    day = int(file_name[-7:-5])
    return year, month, day


def _generate_html_file(settings, input_file, output_file, previous_page, next_page):
    with open(input_file) as f:
        try:
            data = yaml.safe_load(f.read().decode('utf8'))
        except:
            import traceback
            sys.stderr.write('DiaryFormatError: "' + input_file + '"\n')
            traceback.print_exc(0)
            data = ''

    image_folder = settings['image_folder']
    output_folder = settings['output_folder']
    name = settings['player']

    year, month, day = _parse_date(input_file)

    body_text = ''
    for block in data:
        for key, value in block.items():
            if key == 'image':
                body_text += _place_image(image_folder, value, output_folder)
            elif key == 'draft':
                body_text += _place_draft(value, name)
            elif key == 'title':
                body_text += _place_title(value)
            elif key == 'text':
                body_text += _place_text(value)
            elif key == 'link':
                body_text += _place_link(value)
            elif key == 'rank':
                _parse_rank_icon(value)
            else:
                print('UnknownTokenError: ' + block)

    output = ''
    title = u'HotS にっき(' + str(month) + '/' + str(day) + ')'
    output += _place_header(title)
    output += _place_links(previous_page, next_page)
    output += '<div class="diary">'
    output += _place_date(month, day, name)

    output += body_text

    output += '</div>'
    output += _place_links(previous_page, next_page)
    output += _place_footer()
    return output


def _season_start_html(season_name):
    output = ''
    output += '<h3 class="season">' + season_name + '</h3>'
    output += '<ul class="season">'
    return output

def _season_end_html():
    return '</ul>'


def _season_data(index):
    seasons = [
        [ '2017 Season 3', datetime.datetime(2017, 12, 12) ],
        [ '2018 Season 1', datetime.datetime(2018, 3, 6) ],
        [ '2018 Season 2', datetime.datetime(2018, 7, 9) ],
        [ '2018 Season 3', datetime.datetime(2018, 9, 24) ],
    ]
    return seasons[index]


def _generate_index_file(pages, output_folder):
    header = ''
    header += _place_header(u'HotS にっき')
    header += '<div class="diary">'
    header += u'<h1>HotS にっき</h1>'

    season_links = []

    season_index = 0
    season_name, season_end = _season_data(season_index)
    links = _season_start_html(season_name)
    for page in pages:
        year, month, day = _parse_date(page)

        date = datetime.datetime(year, month, day)
        if date > season_end:
            links += _season_end_html()
            season_links.append(links)

            season_index += 1
            season_name, season_end = _season_data(season_index)
            links = _season_start_html(season_name)

        links += '<li><a href="' + page + '">' + str(month) + u' がつ ' + str(day) + u' にち</a></li>'

    links += _season_end_html()
    season_links.append(links)

    footer = ''
    footer += u'<br>うずらフォントのインストールを強くおすすめします。 → <a href="http://azukifont.com/font/uzura.html">うずらフォント配布ページ</a>'

    footer += '</div>'
    footer += _place_footer()

    body = '<table><tr>'
    season_links.reverse()
    for links in season_links:
        body += '<td class="season">' + links + '</td>'
    body += '</tr></table>'
    output = header + body + footer

    output_file = os.path.join(output_folder, 'index.html')
    with codecs.open(output_file, 'w', 'utf-8') as f:
        f.write(output)


with open('settings.yaml') as f:
    settings = yaml.safe_load(f.read().decode('utf8'))

data_folder = settings['data_folder']
output_folder = settings['output_folder']

try:
    input_files = os.listdir(data_folder)
except:
    print 'Error: not found diary files'
    sys.exit()

diary_files = []
for file_name in input_files:
    basename, ext = os.path.splitext(file_name)
    if ext.lower() == '.yaml':
        diary_files.append(basename + '.yaml')

previous_page = None
previous_pages = []
next_pages = []
pages = []
for i in range(len(diary_files)):
    basename, ext = os.path.splitext(diary_files[i])
    current_page = basename + '.html'
    pages.append(current_page)
    previous_pages.append(previous_page)
    if (i + 1) < len(diary_files):
        next_basename, ext = os.path.splitext(diary_files[i + 1])
        next_pages.append(next_basename + '.html')
    previous_page = current_page
next_pages.append(None)

for i in range(len(diary_files)):
    output_file = os.path.join(output_folder, pages[i])
    input_file = os.path.join(data_folder, diary_files[i])
    html_text = _generate_html_file(settings, input_file, output_file, previous_pages[i], next_pages[i])
    with codecs.open(output_file, 'w', 'utf-8') as f:
        f.write(html_text)

_generate_index_file(pages, output_folder)
